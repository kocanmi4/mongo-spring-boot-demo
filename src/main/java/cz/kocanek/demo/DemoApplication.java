package cz.kocanek.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.List;

@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

}
/**
 * Usually controller is separated in different package, but for better readability it is included here.
 */
@RestController
class CustomerController {

	private final CustomerRepository repository;

	CustomerController(CustomerRepository repository) {
		this.repository = repository;
	}

	@GetMapping("/customers")
	public List<Customer> getCustomers() {
		return repository.findAll();
	}

	@PostMapping("/customers")
	public Customer saveCustomer(@RequestBody Customer customer) {
		return repository.save(customer);
	}

}
/**
 * Usually repository is separated in different package, but for better readability it is included here.
 */
@Document("customers")
record Customer(@Id String id, String name, List<Vehicle> vehicles) {}
record Vehicle(String model, String vin, Integer mileage) {}
/**
 * Usually model class is separated in different package, but for better readability it is included here.
 */
@Repository
interface CustomerRepository extends MongoRepository<Customer, String> {}